import CoreData
import Foundation

/// An environment singleton responsible for managin our Core Data stack.
class DataController: ObservableObject {
    /// The lone CloudKit container used to store all our data.
    let container: NSPersistentCloudKitContainer
    
    /// Loads the data model exactly once. We need to make the init method uses this model rather than have it
    /// attempt to load the model itself. This prevent from two models being loaded at the same time when testing.
    static let model: NSManagedObjectModel = {
        guard let url = Bundle.main.url(forResource: "RecordModel", withExtension: "momd") else {
            fatalError("Failed to locate model file.")
        }

        guard let managedObjectModel = NSManagedObjectModel(contentsOf: url) else {
            fatalError("Failed to load model file.")
        }

        return managedObjectModel
    }()

    /// Initializes a data controller, either in memory (for temporary use such as testing and previewing),
    /// or on permanent storage (for use in regular app runs).
    ///
    /// Defaults to permanent storage.
    /// - Parameter inMemory: Whether to store this data in temporary memory or not.
    init(inMemory: Bool = false) {
        container = NSPersistentCloudKitContainer(name: "RecordModel", managedObjectModel: Self.model)
        
        // For testing and previewing purpose, we create a temporary, in-memory database by
        // writing to /dev/null so our data is destroyed after the app finishes running.
        if inMemory {
            container.persistentStoreDescriptions.first?.url = URL(fileURLWithPath: "/dev/null")
        }
        
        container.loadPersistentStores { _, error in
            if let error {
                fatalError("Failed to load the data: \(error.localizedDescription)")
            }
            self.container.viewContext.automaticallyMergesChangesFromParent = true
        }
    }
    
    func createSampleData() throws {
        let viewContext = container.viewContext
        
        let currentDate = Date()
        var dateComponents = DateComponents()
        
        for recordIndex in 1...10 {
            dateComponents.day = recordIndex
            let record = Record(context: viewContext)
            record.shift = String(recordIndex)
            record.date = Calendar.current.date(byAdding: dateComponents, to: currentDate)
            record.duration = Int16(400 + recordIndex)
        }
        
        try viewContext.save()
    }
    
    func save() {
        do {
            if container.viewContext.hasChanges {
                try container.viewContext.save()
                print("Data saved!!")
            }
        } catch {
            print("We could not save the data...")
        }
    }
    
//    func addRecord(shift: String, date: Date, duration: Int, context: NSManagedObjectContext) {
//        let record = Record(context: context)
//        record.shift = shift
//        record.date = date
//        record.duration = Int16(duration)
//
//        save(context: context)
//    }
    
//    func editRecord(record: Record, shift: String, date: Date, duration: Int, context: NSManagedObjectContext) {
//        record.shift = shift
//        record.date = date
//        record.duration = Int16(duration)
//
//        save(context: context)
//    }
    
//    func delete(_ object: Record, context: NSManagedObjectContext) {
//        context.delete(object)
//    }

    static var preview: DataController = {
        let dataController = DataController(inMemory: true)
        let viewContext = dataController.container.viewContext
        
        do {
            try dataController.createSampleData()
        } catch {
            fatalError("Fatal error creating preview: \(error.localizedDescription)")
        }
        
        return dataController
    }()
}
