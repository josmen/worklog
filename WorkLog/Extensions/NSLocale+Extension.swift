import Foundation

extension NSLocale {
    @objc static let currentLocale = NSLocale(localeIdentifier: "es_ES")
}
