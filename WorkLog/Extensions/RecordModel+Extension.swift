import Foundation

extension Record {
    var recordShift: String {
        shift ?? "N/A"
    }
    
    var recordDate: Date {
        date ?? Date()
    }
    
    var recordDuration: Int16 {
        duration
    }
    
    static var example: Record {
        let controller = DataController.preview
        let viewContext = controller.container.viewContext
        
        let record = Record(context: viewContext)
        record.shift = "6"
        record.date = Date()
        record.duration = Int16(400)
        record.extra = Int16(30)
        
        return record
    }
}
