import Foundation

func getDurationInMinutes(from start: String, to end: String) -> Int16 {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm"
    
    let startTime = dateFormatter.date(from: start)!
    let endTime = dateFormatter.date(from: end)!
    
    let calendar = Calendar.current
    let startTimeComponents = calendar.dateComponents([.hour, .minute], from: startTime)
    let endTimeComponents = calendar.dateComponents([.hour, .minute], from: endTime)
    let difference = calendar.dateComponents([.minute], from: startTimeComponents, to: endTimeComponents).minute!
    
    return Int16(difference)
}

func getHoursAndMinutes(from minutes: Int16) -> String {
    let hours = minutes / 60
    let leftMinutes = minutes % 60
    
    if leftMinutes < 10 {
        return "\(hours):0\(leftMinutes)"
    }
    
    return "\(hours):\(leftMinutes)"
}
