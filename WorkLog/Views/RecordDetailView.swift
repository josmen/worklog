import SwiftUI

struct RecordDetailView: View {
    @ObservedObject var record: Record
    
    @State private var isShowingEditRecord = false
    
    var body: some View {
        VStack(spacing: 10) {
            Text("Turno \(record.recordShift)")
                .font(.largeTitle)
                .bold()
            Text("\(record.recordDate.formatted(date: .abbreviated, time: .omitted))")
                .font(.title3)
                .padding(.bottom, 20)
            Text("Horas de cómputo \(getHoursAndMinutes(from: record.duration))")
                .foregroundColor(.secondary)
            if record.extra > 0 {
                Text("Horas exta \(getHoursAndMinutes(from: record.extra))")
                    .foregroundColor(.secondary)
            }
        }
        .frame(width: 300, height: 250)
        .toolbar {
            ToolbarItem {
                Button {
                    isShowingEditRecord = true
                } label: {
                    Label("Editar", systemImage: "square.and.pencil")
                }

            }
        }
        .sheet(isPresented: $isShowingEditRecord) {
            EditRecordView(record: record)
        }
    }
}

struct RecordDetailView_Previews: PreviewProvider {
    static var previews: some View {
        RecordDetailView(record: Record.example)
    }
}
