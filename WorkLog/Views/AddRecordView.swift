import SwiftUI

struct AddRecordView: View {
    @EnvironmentObject var dataController: DataController
    @EnvironmentObject var shiftsVM: ShiftsViewModel
    
    @Environment(\.dismiss) var dismiss
    
    @State private var date = Date()
    @State private var selectedShift = "1"
    @State private var extraTime: Double = 0
    
    var body: some View {
        NavigationView {
            Form {
                DatePicker("Fecha", selection: $date, displayedComponents: .date)

                Picker("Turno", selection: $selectedShift) {
                    ForEach(shiftsVM.actualShiftsNames, id: \.self) { shiftName in
                        Text("Turno \(shiftName)")
                    }
                }
                
                Section("Exceso de jornada") {
                    Text("\(extraTime.formatted()) minutos")
                    Slider(value: $extraTime, in: 0...120, step: 1)
                }
            }
            .navigationTitle("Registro turno")
            .toolbar {
                Button("Guardar") {
                    addRecord()
                    dismiss()
                }
            }
        }
    }
}

struct AddRecordView_Previews: PreviewProvider {
    static var previews: some View {
        AddRecordView()
            .environmentObject(ShiftsViewModel())
            .environmentObject(DataController())
    }
}

extension AddRecordView {
    func addRecord() {
        let shift = shiftsVM.getShift(selectedShift, date)
        let shiftName = shift.name
        let duration = getDurationInMinutes(from: shift.start, to: shift.end)
        
        let record = Record(context: dataController.container.viewContext)
        record.shift = shiftName
        record.date = date
        record.duration = duration
        record.extra = Int16(extraTime)

        dataController.save()
    }
}
