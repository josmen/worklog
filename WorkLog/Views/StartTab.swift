import SwiftUI

struct StartTab: View {
    @EnvironmentObject var dataController: DataController
    
    var body: some View {
        TabView {
            HomeView(dataController: dataController)
                .tag(HomeView.tag)
                .tabItem {
                    Label("Registro", systemImage: "text.badge.plus")
                }
            
            SummaryView()
                .tag(SummaryView.tag)
                .tabItem {
                    Label("Resumen", systemImage: "list.bullet")
                }
        }
    }
}

struct StartTab_Previews: PreviewProvider {
    static var previews: some View {
        StartTab()
    }
}
