import SwiftUI

struct HomeView: View {
    @StateObject var viewModel: ViewModel
    
    init(dataController: DataController) {
        let viewModel = ViewModel(dataController: dataController)
        _viewModel = StateObject(wrappedValue: viewModel)
    }
    
    static let tag: String? = "Home"
    
    @State private var isShowingAddShift = false
    
    var body: some View {
        NavigationView {
            List(viewModel.records) { record in
                NavigationLink {
                    RecordDetailView(record: record)
                } label: {
                    HStack {
                        Text(record.recordDate.formatted(date: .abbreviated, time: .omitted))
                        Spacer()
                        Text("Turno \(record.recordShift)")
                    }
                }
                .listRowSeparator(.hidden)
            }
            .navigationTitle("Registro turno")
            .listStyle(.plain)
            .toolbar {
                Button {
                    isShowingAddShift = true
                } label: {
                    Image(systemName: "plus")
                }
                
            }
            .sheet(isPresented: $isShowingAddShift) {
                AddRecordView()
            }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(dataController: .preview)
    }
}

