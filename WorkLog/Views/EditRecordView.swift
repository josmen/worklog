
import SwiftUI

struct EditRecordView: View {
    @ObservedObject var record: Record
    
    @EnvironmentObject var dataController: DataController
    @EnvironmentObject var shiftsVM: ShiftsViewModel
    
    @Environment(\.dismiss) var dismiss
    
    @State private var date: Date
    @State private var selectedShift: String
    @State private var extraTime: Double = 0
    
    init(record: Record) {
        self.record = record
        
        _date = State(wrappedValue: record.recordDate)
        _selectedShift = State(wrappedValue: record.recordShift)
    }
    
    var body: some View {
        Form {
            Picker("Turno", selection: $selectedShift) {
                ForEach(shiftsVM.actualShiftsNames, id: \.self) { shift in
                    Text("Turno \(shift)")
                }
            }
            .onAppear {
                date = record.date!
                selectedShift = record.shift!
                extraTime = Double(record.extra)
            }
            
            DatePicker("Fecha", selection: $date, displayedComponents: .date)
            
            Section("Exceso de jornada") {
                Text("\(extraTime.formatted()) minutos")
                Slider(value: $extraTime, in: 0...120, step: 1)
            }
            
            Button("Guardar cambios", action: update)
        }
    }
}

struct EditRecordView_Previews: PreviewProvider {
    static var previews: some View {
        EditRecordView(record: Record.example)
            .environmentObject(DataController())
            .environmentObject(ShiftsViewModel())
    }
}

extension EditRecordView {
    func update() {
        let shift = shiftsVM.getShift(selectedShift, date)

        record.shift = selectedShift
        record.date = date
        record.duration = Int16(getDurationInMinutes(from: shift.start, to: shift.end))
        record.extra = Int16(extraTime)
        dismiss()
    }
}
