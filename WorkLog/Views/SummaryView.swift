import SwiftUI

struct SummaryView: View {
    @FetchRequest(sortDescriptors: []) private var record: FetchedResults<Record>
    private let hoursToWork: Double = 1567
    
    static let tag: String? = "Summary"

    var body: some View {
        NavigationView {
            VStack {
                ProgressView(value: totalTime, total: hoursToWork) {
                    Label(String(format: "He trabajado %.2f horas de \(hoursToWork)", totalTime), systemImage: "clock")
                }
                .padding()
            }
            .navigationTitle("Resumen")
        }
    }
}

struct SummaryView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            SummaryView()
        }
    }
}

extension SummaryView {
    var totalTime: Double {
        var totalTime = 0
        
        for rec in record {
            totalTime += Int(rec.duration)
        }
        
        return Double(totalTime) / 60
    }
}
