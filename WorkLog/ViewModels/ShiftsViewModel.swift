import Foundation

class ShiftsViewModel: ObservableObject {
    let shifts: [Shift]
    
    var actualShifts: [Shift] {
        let actualShitsDate = shifts.sorted().last?.validFrom
        return shifts.filter { $0.validFrom == actualShitsDate }
    }
    var actualShiftsNames: [String] {
        actualShifts.map { $0.name }
    }
    
    init() {
        shifts = Bundle.main.decode("shifts.json")
    }
    
    func getShift(_ name: String, _ date: Date) -> Shift {
        let shiftsWithSameName = shifts.filter { $0.name == name }
        
        var selectedShift: Shift?
        
        for shift in shiftsWithSameName {
            if shift.validFrom <= date {
                selectedShift = shift
            }
        }
        
        return selectedShift!
    }
}
