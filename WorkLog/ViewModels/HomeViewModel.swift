import CoreData
import Foundation

extension HomeView {
    class ViewModel: NSObject, ObservableObject, NSFetchedResultsControllerDelegate {
        private let viewController: NSFetchedResultsController<Record>
        
        @Published var records = [Record]()
        
        var dataController: DataController
        
        init(dataController: DataController) {
            self.dataController = dataController
            
            let request: NSFetchRequest<Record> = Record.fetchRequest()
            request.sortDescriptors = [
                NSSortDescriptor(keyPath: \Record.date, ascending: false),
                NSSortDescriptor(keyPath: \Record.shift, ascending: true)
            ]
            
            viewController = NSFetchedResultsController(
                fetchRequest: request,
                managedObjectContext: dataController.container.viewContext,
                sectionNameKeyPath: nil,
                cacheName: nil)
            
            super.init()
            
            viewController.delegate = self
            
            do {
                try viewController.performFetch()
                records = viewController.fetchedObjects ?? []
            } catch {
                print("Failed to fetch initial data.")
            }
        }
        
        func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
            if let newItems = controller.fetchedObjects as? [Record] {
                records = newItems
            }
        }
    }
}
