import Foundation

struct Shift: Codable, Comparable, Hashable {
    let name: String
    let start: String
    let end: String
    let validFrom: Date
    
    var id: String {
        "\(name)_\(validFrom)"
    }
    
    static func < (lhs: Shift, rhs: Shift) -> Bool {
        lhs.validFrom < rhs.validFrom
    }
}
