import SwiftUI

@main
struct AppEntry: App {
    @StateObject var dataController: DataController
    @StateObject var shiftsVM = ShiftsViewModel()
    
    init() {
        let dataController = DataController()
        _dataController = StateObject(wrappedValue: dataController)
    }
    
    var body: some Scene {
        WindowGroup {
            StartTab()
                .environment(\.managedObjectContext, dataController.container.viewContext)
                .environmentObject(dataController)
                .environmentObject(shiftsVM)
        }
    }
}
